//
//  RPURLConnection.h
//  foothold
//
//  Created by Ramprakash Goyal on 29/07/14.
//  Copyright (c) 2014 mac3. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPURLConnection : NSURLConnection


@property int tag;
@property double expectedLength;
@property double currentLength;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSDictionary *dictData;
@property BOOL isMain;
@end
