//
//  NetworkUtills.m
//  IFINotes
//
//  Created by Rajendra soni on 2/29/12.
//  Copyright (c) 2012 Fox Infosoft. All rights reserved.
//

#import "NetworkUtills.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SohanAlert.h"
#import "Reachability.h"
#import "SVProgressHUD.h"

@implementation NetworkUtills

@synthesize tag,strResponse;
// to Check that internet is available or not

-(NetworkUtills *)initWithSelector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    self=[super init];
    myAlert=[[SohanAlert alloc] init];
    callbackSelector=selector;
    CallBackObject=objcallbackObject;
    return self;
}
-(BOOL)isInternetAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];  
    NetworkStatus networkStatus = [reachability currentReachabilityStatus]; 
    if (networkStatus==NotReachable) 
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning !!" message:@"Your Internet connection is not active at this time. Try on the Internet is available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        alert=nil;
        return NO;
    }
    return YES;
}
-(void)RequestFinished
{
    if (CallBackObject)
    {
        [CallBackObject performSelector:callbackSelector withObject:self afterDelay:0.0];
    }
}
-(void)GetResponseByASIHttpRequest:(NSString *)strURL
{
    NSLog(@"Requested URL is %@",strURL);
    if ([self isInternetAvailable])
    {
        //[myAlert showAlert];
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
        strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
          NSLog(@"Requested URL is %@",strURL);
        NSURL *url=[NSURL URLWithString:strURL];
        ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
        [request setDidFailSelector:@selector(ASIHTTPRequestRequestFailed:)];
        [request setDidFinishSelector:@selector(ASIHTTPRequestRequestFinished:)];
        [request setTimeOutSeconds:30.0];
        [request startAsynchronous];
        
    }
    
}


#pragma  ASIHTTPRequest request delegate
-(void)ASIHTTPRequestRequestFinished:(ASIHTTPRequest *)request
{
    //[myAlert hideAlert];
    [SVProgressHUD dismiss];
	self.strResponse = [request responseString];
	NSLog(@"Response : %@",strResponse);
    [self RequestFinished];
}
- (void)ASIHTTPRequestRequestFailed:(ASIHTTPRequest *)request
{
   // [myAlert hideAlert];
    [SVProgressHUD dismiss];
	NSError *error = [request error];
	NSString *errorString = [error localizedDescription];
	NSLog(@"Error %@",errorString);
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
   // [self RequestFinished];
    
}
-(void)GetResponseByASIFormDataRequest:(NSString *)strURL WithDictionary:(NSDictionary *)dictPostParamas
{
    NSLog(@"Requested URL is %@",strURL);
    if ([self isInternetAvailable])
    {
        //[myAlert showAlert];
        
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
        strURL=[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url=[NSURL URLWithString:strURL];
        ASIFormDataRequest *request=[ASIFormDataRequest requestWithURL:url];
        [request setDelegate:self];
        [request setDidFailSelector:@selector(ASIFormDataRequestRequestRequestFailed:)];
        [request setDidFinishSelector:@selector(ASIFormDataRequestRequestRequestFinished:)];
        [request setTimeOutSeconds:30.0];
        NSArray *arrAllKeys=[dictPostParamas allKeys];
        for (int i=0; i<[arrAllKeys count]; i++) 
        {
            NSString *strKeyName=[arrAllKeys objectAtIndex:i];
            if ([[dictPostParamas objectForKey:strKeyName] isKindOfClass:[UIImage class]]) 
            {
                UIImage *img=[dictPostParamas objectForKey:strKeyName];
                NSData *data=UIImageJPEGRepresentation(img, 1.0);
                [request setData:data forKey:strKeyName];
            }
            else if ([strKeyName isEqualToString:@"progress"])
            {
//                MDRadialProgressView *radialView=(MDRadialProgressView *)[dictPostParamas objectForKey:strKeyName];
//                [request setDownloadProgressDelegate:radialView.progressCounter];
            }
            else if ([[dictPostParamas objectForKey:strKeyName] isEqualToString:@"image"])
            {
               
                [request setFile:[dictPostParamas objectForKey:strKeyName] forKey:strKeyName];
            }
            

            else
            {
                [request setPostValue:[dictPostParamas objectForKey:strKeyName] forKey:strKeyName];
            }
            
            
        }
        [request startAsynchronous];
    }
    else
    {
        [self RequestFinished];
    }
    
}


#pragma Login ASIHTTPRequest request delegate
-(void)ASIFormDataRequestRequestRequestFinished:(ASIFormDataRequest *)request
{
   // [myAlert hideAlert];
    [SVProgressHUD dismiss];
	self.strResponse = [request responseString];
	NSLog(@"Response : %@",strResponse);
    [self RequestFinished];
}
- (void)ASIFormDataRequestRequestRequestFailed:(ASIFormDataRequest *)request
{
    //[myAlert hideAlert];
    [SVProgressHUD dismiss];
	NSError *error = [request error];
	NSString *errorString = [error localizedDescription];
	NSLog(@"Error %@",errorString);
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
    //[self RequestFinished];
    
}

@end
