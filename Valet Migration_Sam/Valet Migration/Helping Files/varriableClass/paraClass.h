//
//  paraClass.h
//  Cab
//
//  Created by Admin on 08/12/16.
//  Copyright © 2016 Centreonyx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface paraClass : NSObject

//******UI
+(NSString *)dateformat:(NSString*)datestring;
+(void )AlertView:(NSString*)message;
+(NSString *)uniqueId;

+(void)lblBorderWidth:(UILabel *)lbl;
+(void)btnBorderWidth:(UIButton *)buttonView;
+(void)imgBorderWidth:(UIImageView *)imageView;
+(void)viewBorderWidth:(UIView *)imageView;

+(BOOL) NSStringIsValidEmail:(NSString *)checkString;

//*********** View controller
//+(void)SiginView :(id)class;
//+(void)SigupView :(id)class;
//+(void)MainHomeView :(id)class;

@end
