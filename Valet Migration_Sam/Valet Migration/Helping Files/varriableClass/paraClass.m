//
//  paraClass.m
//  Cab
//
//  Created by Admin on 08/12/16.
//  Copyright © 2016 Centreonyx. All rights reserved.
//

#import "paraClass.h"
#import <UIKit/UIKit.h>

@implementation paraClass

- (id)init
{
    self = [super init];
    if (self)
    {
        
        
    }
    return self;
}

#pragma mark Date Formatter

+(NSString*)uniqueId
{
  return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+(NSString*)dateformat:(NSString *)datestring
{
    NSString*inputdate=datestring;
    NSDateFormatter*dateformat=[[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate*date=[dateformat dateFromString:inputdate];
    [dateformat setDateFormat:@"dd-MM-yyyy"];
    datestring = [NSString stringWithFormat:@"%@",[dateformat stringFromDate:date]];
    return datestring;
}

#pragma mark Alert View

+(void )AlertView:(NSString*)message
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Cab" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - borderWidth
+(void)lblBorderWidth:(UILabel *)lbl
{
    lbl.layer.cornerRadius = 10;
    lbl.layer.borderWidth = 1;
    lbl.layer.borderColor = [[UIColor blackColor] CGColor];
}


+(void)btnBorderWidth:(UIButton *)buttonView
{
    buttonView.layer.cornerRadius = 10;
    buttonView.layer.borderWidth = 1;
    buttonView.layer.borderColor = [[UIColor blackColor] CGColor];
}

+(void)imgBorderWidth:(UIImageView *)imageView
{
    //imageView.layer.cornerRadius = 10;
    imageView.layer.borderWidth = 1;
    imageView.layer.borderColor = [[UIColor blackColor] CGColor];
}

+(void)viewBorderWidth:(UIView *)givenView
{
    givenView.layer.cornerRadius = 10;
    givenView.layer.borderWidth = 1;
    givenView.layer.borderColor = [[UIColor blackColor] CGColor];
    givenView.layer.masksToBounds = YES;
}

#pragma mark - NSStringIsValidEmail
+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


#pragma mark View Controller
//+(void)MainHomeView:(id)class
//{
//    
//}

@end
