//
//  UrlClass.h
//  Cab
//
//  Created by Admin on 08/12/16.
//  Copyright © 2016 Centreonyx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "Paraclass.h"

@interface UrlClass : NSObject <NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionDownloadDelegate,ASIHTTPRequestDelegate>
{
    SEL callbackSelector;
    id  CallBackObject;
    NSURLSessionDataTask* datatask;
}

@property (strong,nonatomic) NSString*Apikeyresponse;

-(void)CancelURl:(NSString *)geturlstr;
-(void)StartURl:(NSString *)geturlstr;

-(void)HttpsGetFromServer:(NSString*)geturlstr;
-(void)HttpsPostToServer:(NSString *)posturl :(NSString *)parameters;
-(void)HttpsDownloadFiles:(NSString *)downloadlink;

-(void)HttpsUploadFiles:(NSString *)posturl :(NSString *)parameters : (NSString *)fileparameter  :(NSArray *)dataArr;

-(void)RequestFinished;

-(UrlClass *)initWithSelector:(SEL )selector WithCallBackObject:(id)objcallbackObject;

@end
