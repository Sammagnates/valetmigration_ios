//
//  UrlClass.m
//  Cab
//
//  Created by Admin on 08/12/16.
//  Copyright © 2016 Centreonyx. All rights reserved.
//

#import "UrlClass.h"
#import "JSON.h"

@implementation UrlClass

-(UrlClass *)initWithSelector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    self=[super init];
    
    callbackSelector=selector;
    CallBackObject=objcallbackObject;
    return self;
}

-(BOOL)isInternetAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus==NotReachable)
        
    {
        [SVProgressHUD dismiss];
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning!!" message:@"Your Internet connection is not active at this time. Try to Internet is available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}




-(void)HttpsGetFromServer:(NSString *)geturlstr
{
    
    
    if ([self isInternetAvailable])
    {
        
        //  [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeClear];
        NSURLSessionConfiguration*defaultConfigObject=[NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession=[NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue:nil];
        
        NSURL*url=[NSURL URLWithString:geturlstr];
        datatask= [delegateFreeSession dataTaskWithURL:url];
        
        [datatask resume];
        
    }
    
}
-(void)CancelURl:(NSString *)geturlstr
{
    
    if ([self isInternetAvailable])
    {
        NSURLSessionConfiguration*defaultConfigObject=[NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession=[NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue:nil];
        
        NSURL*url=[NSURL URLWithString:geturlstr];
        datatask= [delegateFreeSession dataTaskWithURL:url];
        [datatask cancel];
        
        
        
    }
    
    
}
-(void)StartURl:(NSString *)geturlstr;
{
    
    if ([self isInternetAvailable])
    {
        NSURLSessionConfiguration*defaultConfigObject=[NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession=[NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue:nil];
        
        NSURL*url=[NSURL URLWithString:geturlstr];
        datatask= [delegateFreeSession dataTaskWithURL:url];
        [datatask resume];
        
        
        
    }
    
    
}

- (void)startSecureConnection
{
    [datatask resume];
}

- (void)stopSecureConnection
{
    [datatask suspend];
}

-(void)HttpsPostToServer:(NSString *)posturl :(NSString *)parameters

{
    if ([self isInternetAvailable])
    {
        
        //    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeClear];
        
        NSURLSessionConfiguration*defaultConfigObject=[NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession=[NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue:nil];
        
        posturl=[posturl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL*url=[NSURL URLWithString:posturl];
        NSMutableURLRequest*urlrequest=[NSMutableURLRequest requestWithURL:url];
        NSString*params=parameters;
        [urlrequest setHTTPMethod:@"POST"];
        
        
        [urlrequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        datatask= [delegateFreeSession dataTaskWithRequest:urlrequest];
        [datatask resume];
        
    }
    
}

-(void)HttpsUploadFiles:(NSString *)posturl :(NSString *)parameters :(NSString *)fileparameter  :(NSArray *)dataArr;
{
    // put image in array, image data is creating in this method
    if ([self isInternetAvailable])
    {
        NSString* strurl=[NSString stringWithFormat:@"%@%@",posturl,parameters];
        
        strurl=[strurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *url = [NSURL URLWithString:strurl];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setDelegate:self];
        
        NSLog(@"dataArr: %@",dataArr);
        
        for (int i = 0; i < [dataArr count]; i++)
        {
            [request setData:UIImagePNGRepresentation([dataArr objectAtIndex:i]) withFileName:@"image.png" andContentType:@"image/png" forKey:[NSString stringWithFormat:@"%@%d",fileparameter,i + 1]];
        }
        
        //  can use for add text parametrs
        //[request appendPostData:[@"This is my data" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [SVProgressHUD showWithStatus:@"Uploading Please Wait..." maskType: SVProgressHUDMaskTypeBlack];
        [request startAsynchronous];
        
    }
    
}



-(void)HttpsDownloadFiles:(NSString *)downloadlink
{
    
    NSURL *url = [NSURL URLWithString:downloadlink];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDownloadTask * downloadTask =[ defaultSession downloadTaskWithURL:url];
    [downloadTask resume];
}


#pragma mark Http Delegates

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    [SVProgressHUD dismiss];
    
    
    
    NSLog(@"Response %d : %@", request.responseStatusCode, [request responseString]);
    
    self.Apikeyresponse=[NSString stringWithFormat:@"%@",[request responseString]];
    
    
    [self RequestFinished];
    
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    
    [SVProgressHUD dismiss];
    self.Apikeyresponse=[NSString stringWithFormat:@"%@",[request responseString]];
    
    
    
    [self RequestFinished];
    
    
}


#pragma mark Session Delegate



- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask
{
    
    
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    //  NSLog(@"### handler 1");
    
    completionHandler(NSURLSessionResponseAllow);
    
    
    
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    [SVProgressHUD dismiss];
    NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received String %@",str);
    
    self.Apikeyresponse=str;
    
    
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(error == nil)
    {     [SVProgressHUD dismiss];
        [self RequestFinished];
        
        
    }
    else
    {
        //  NSLog(@"Error %@",[[error userInfo] objectForKey:@"NSLocalizedDescription"]);
        [SVProgressHUD dismiss];
        
        self.Apikeyresponse=[NSString stringWithFormat:@"%@",[[error userInfo] objectForKey:@"NSLocalizedDescription"]];
        
        
        [self performSelectorOnMainThread:@selector(alertmsg:) withObject:nil waitUntilDone:YES];
        [self RequestFinished];
        
        
        
        
    }
    
    
    
}
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    // NSLog(@"Temporary File :%@\n", location);
    NSError *err = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    NSURL *docsDirURL = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:@"out1.zip"]];
    if ([fileManager moveItemAtURL:location
                             toURL:docsDirURL
                             error: &err])
    {
        
        self.Apikeyresponse=[NSString stringWithFormat:@"Downloaded file location url:- %@" ,docsDir ];
        
    }
    else
    {
        
        
        
        
        self.Apikeyresponse=[NSString stringWithFormat:@"%@" ,[[err userInfo] objectForKey:@"NSLocalizedDescription"] ];
        
        [self performSelectorOnMainThread:@selector(alertmsg:) withObject:nil waitUntilDone:YES];
        
        [self RequestFinished];
        
        
    }
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    //  float progress = totalBytesWritten*1.0/totalBytesExpectedToWrite;
    dispatch_async(dispatch_get_main_queue(),^ {
        // [self.progress setProgress:progress animated:YES];
    });
    //  NSLog(@"Progress =%f",progress);
    //  NSLog(@"Received: %lld bytes (Downloaded: %lld bytes)  Expected: %lld bytes.\n",
    //          bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
}
-(void)RequestFinished
{
    if (CallBackObject)
    {
        [CallBackObject performSelectorOnMainThread:callbackSelector withObject:self waitUntilDone:YES];
    }
}


-(void)alertmsg : (NSString *) str
{
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error !!" message:self.Apikeyresponse delegate:nil cancelButtonTitle:@"Try Later" otherButtonTitles:nil, nil];
    [alert show];
    
    
    
}


@end
