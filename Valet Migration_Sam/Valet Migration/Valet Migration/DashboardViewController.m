//
//  DashboardViewController.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "DashboardViewController.h"
#import "AppDelegate.h"
#import "AboutusViewController.h"
#import "GalleryViewController.h"
#import "MapViewController.h"
#import "OffersViewController.h"
#import "ShareViewController.h"
#import "AppointmentViewController.h"
#import "UrlClass.h"
#import "paraClass.h"
#import "JSON.h"
#import "BlogVC.h"

#define IS_IPHONE (!IS_IPAD)
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface DashboardViewController ()
{
    int flag;
    NSString *msgStr;
    UrlClass *nativeclass;
}

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIImageView *animationShowHideImg;
@property (strong, nonatomic) IBOutlet UIImageView *animationPlusImg;
@property (strong, nonatomic) IBOutlet UIImageView *logoImg;

@property (strong, nonatomic) IBOutlet UIButton *plusBtn;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;

@property (strong, nonatomic) IBOutlet UIButton *offersBtn;
@property (strong, nonatomic) IBOutlet UIButton *blogbtn;
@property (strong, nonatomic) IBOutlet UIButton *mapBtn;
@property (strong, nonatomic) IBOutlet UIButton *shareBtn;
@property (strong, nonatomic) IBOutlet UIButton *callBtn;
@property (strong, nonatomic) IBOutlet UIButton *galleryBtn;
@property (strong, nonatomic) IBOutlet UIButton *aboutBtn;
@property (strong, nonatomic) IBOutlet UIButton *appointmentBtn;
@property (strong, nonatomic) IBOutlet UIView *animationView;
@property (weak, nonatomic) IBOutlet UILabel *lblBlog;

@property (strong, nonatomic) IBOutlet UILabel *offerLbl;
@property (strong, nonatomic) IBOutlet UILabel *mapLbl;

@property (strong, nonatomic) IBOutlet UILabel *shareLbl;
@property (strong, nonatomic) IBOutlet UILabel *callLbl;
@property (strong, nonatomic) IBOutlet UILabel *galleryLbl;
@property (strong, nonatomic) IBOutlet UILabel *aboutLbl;
@property (strong, nonatomic) IBOutlet UILabel *appointmentLbl;

@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UILabel *logoLbl;
@property (weak, nonatomic) IBOutlet UIImageView *appBackgroundView;

@end

@implementation DashboardViewController

#pragma mark - viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.appDelegate getAdzmaniaData];
    
    flag=1;
    
    
    self.headerLbl.font = [UIFont fontWithName:@"MyriadPro-Bold" size:16];
    self.logoLbl.font = [UIFont fontWithName:@"MyriadPro-Regular" size:23];
    
    //self.animationShowHideImg.image=[UIImage imageNamed:@"home_0008_add.png"];
    
    [self.animationPlusImg setHidden:YES];
    [self.animationShowHideImg setHidden:YES];
    
    [self.plusBtn setHidden:YES];
    [self.closeBtn setHidden:YES];
    
    msgStr = @"";
    
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    NSString *isTopicCreated = [defauls objectForKey:@"suscribed"];
    
//    if(isTopicCreated == nil) {
//        NSString *token = [[FIRInstanceID instanceID] token];
//        NSLog(@"InstanceID token: %@", token);
//        [[FIRMessaging messaging] subscribeToTopic:@"/topics/valet"];
//        NSLog(@"Subscribed to valet topic");
//        
//        [defauls setObject:@"YES" forKey:@"suscribed"];
//    }
    [self getBGImage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        self.headerLbl.hidden = true;
    }
    
   // if (!IS_IPHONE) {
       //
   // }
    
}
#pragma mark - viewDidAppear
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSLog(@"self.animationView.frame.size.width: %f",self.animationView.frame.size.width);
   
    NSLog(@"animateAgain %@", self.appDelegate.animateAgain);
    if([self.appDelegate.animateAgain isEqualToString:@"YES"])
    {
        [self.animationPlusImg setHidden:NO];
        [self.animationShowHideImg setHidden:YES];
        
        [self.plusBtn setHidden:NO];
        [self.closeBtn setHidden:YES];
        
        self.offerLbl.hidden=YES;
        self.mapLbl.hidden=YES;
        self.lblBlog.hidden = YES;
        self.shareLbl.hidden=YES;
        self.callLbl.hidden=YES;
        self.galleryLbl.hidden=YES;
        self.aboutLbl.hidden=YES;
        self.appointmentLbl.hidden=YES;
        
        self.offersBtn.hidden=YES;
        self.mapBtn.hidden=YES;
        self.blogbtn.hidden=YES;
        self.shareBtn.hidden=YES;
        self.callBtn.hidden=YES;
        self.galleryBtn.hidden=YES;
        self.aboutBtn.hidden=YES;
        self.appointmentBtn.hidden=YES;
        
        [UIView animateWithDuration:0.5 animations:^{
            self.mapBtn.frame       = CGRectOffset(self.mapBtn.frame, 0, 60);
            self.blogbtn.frame       = CGRectOffset(self.blogbtn.frame, 0, 60);
            self.galleryBtn.frame   = CGRectOffset(self.galleryBtn.frame, 0, -60);
            self.offersBtn.frame    = CGRectOffset(self.offersBtn.frame, 20, 60);
            self.shareBtn.frame     = CGRectOffset(self.shareBtn.frame, -20, 60);
            self.callBtn.frame      = CGRectOffset(self.callBtn.frame, 20, -60);
            self.aboutBtn.frame     = CGRectOffset(self.aboutBtn.frame, -20, -60);
            self.appointmentBtn.frame = CGRectOffset(self.appointmentBtn.frame, -20, -60);
        }];
    }
    else
    {
        [self.animationPlusImg setHidden:YES];
        [self.animationShowHideImg setHidden:NO];
        
        [self.plusBtn setHidden:YES];
        [self.closeBtn setHidden:NO];
    }
    
    if([self.appDelegate.isGoesFromNotification isEqualToString: @"YES"]) {
        self.appDelegate.isGoesFromNotification = @"NO";
        
        [self.animationPlusImg setHidden:NO];
        [self.animationShowHideImg setHidden:YES];
        
        [self.plusBtn setHidden:NO];
        [self.closeBtn setHidden:YES];
    }
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Touch
- (IBAction)action_open_aboutus:(id)sender {
    AboutusViewController *aboutUs = (AboutusViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"aboutusviewcontroller"];
    //[self.navigationController pushViewController:aboutUs animated:YES];
    [self presentViewController:aboutUs animated:YES completion:nil];
}

- (IBAction)action_open_camera:(id)sender {
    GalleryViewController *gallery = (GalleryViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"galleryviewcontroller"];
    [self presentViewController:gallery animated:YES completion:nil];
}

- (IBAction)action_make_call:(id)sender {
    NSLog(@"adzManiaData >>> %@",self.appDelegate.adzManiaData);
    //if (self.appDelegate.adzManiaData[kPhoneNumber]) {
        [self showAlertWithAction:[self.appDelegate.adzManiaData valueForKey:kPhoneNumber]];
    //}
    
}

- (IBAction)action_open_map:(id)sender {
    MapViewController *map = (MapViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"mapviewcontroller"];
    [self presentViewController:map animated:YES completion:nil];
}

- (IBAction)action_open_share:(id)sender {
    ShareViewController *share = (ShareViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"shareviewcontroller"];
    [self presentViewController:share animated:YES completion:nil];
}
- (IBAction)btnOpenBlogClick:(id)sender {
    BlogVC *share = (BlogVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"BlogVC"];
    [self presentViewController:share animated:YES completion:nil];
}

- (IBAction)action_open_special_offers:(id)sender {
    OffersViewController *specialoffers = (OffersViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"offersviewcontroller"];
    [self presentViewController:specialoffers animated:YES completion:nil];
}
- (IBAction)action_open_appointment:(id)sender {
    
    AppointmentViewController *appointment = (AppointmentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AppointmentViewController"];
    //[self.navigationController pushViewController:aboutUs animated:YES];
    [self presentViewController:appointment animated:YES completion:nil];
}
- (IBAction)animationBtnTouch:(id)sender {
    [UIView animateWithDuration:0.35 animations:^{
        //        if(flag==0)
        //        {
        self.offerLbl.hidden=YES;
        self.mapLbl.hidden=YES;
        self.lblBlog.hidden = YES;
        self.shareLbl.hidden=YES;
        self.callLbl.hidden=YES;
        self.galleryLbl.hidden=YES;
        self.aboutLbl.hidden=YES;
        self.appointmentLbl.hidden=YES;
        
        self.mapBtn.frame = CGRectOffset(self.mapBtn.frame, 0, 60);
        self.blogbtn.frame = CGRectOffset(self.blogbtn.frame, 0, 60);
        self.galleryBtn.frame = CGRectOffset(self.galleryBtn.frame, 0, -60);
        self.offersBtn.frame = CGRectOffset(self.offersBtn.frame, 20, 60);
        self.shareBtn.frame = CGRectOffset(self.shareBtn.frame, -20, 60);
        self.callBtn.frame = CGRectOffset(self.callBtn.frame, 20, -60);
        self.aboutBtn.frame = CGRectOffset(self.aboutBtn.frame, -20, -60);
        self.appointmentBtn.frame = CGRectOffset(self.appointmentBtn.frame, -20, -60);
        
        self.offersBtn.alpha = 1;
        self.mapBtn.alpha = 1;
        self.shareBtn.alpha = 1;
        self.callBtn.alpha = 1;
        self.galleryBtn.alpha = 1;
        self.aboutBtn.alpha = 1;
        self.appointmentBtn.alpha = 1;
        self.blogbtn.alpha = 1;
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.offersBtn.alpha = 0;
                             self.mapBtn.alpha = 0;
                             self.shareBtn.alpha = 0;
                             self.callBtn.alpha = 0;
                             self.galleryBtn.alpha = 0;
                             self.aboutBtn.alpha = 0;
                             self.appointmentBtn.alpha = 0;
                             self.blogbtn.alpha = 0;
                         }
        completion:nil];
        
        //self.animationShowHideImg.image=[UIImage imageNamed:@"home_0008_add.png"];
        
        [self.animationPlusImg setHidden:NO];
        [self.animationShowHideImg setHidden:YES];
        
        [self.plusBtn setHidden:NO];
        [self.closeBtn setHidden:YES];
        
        //flag=1;
        //}
    }];
}

- (IBAction)animationPlusBtnTouch:(id)sender {
    [UIView animateWithDuration:0.35 animations:^{
        //        else if(flag==1)
        //        {
        self.offersBtn.alpha = 1;
        self.mapBtn.alpha = 1;
        self.shareBtn.alpha = 1;
        self.callBtn.alpha = 1;
        self.galleryBtn.alpha = 1;
        self.aboutBtn.alpha = 1;
        self.appointmentBtn.alpha = 1;
        self.blogbtn.alpha = 1;
        
        self.offersBtn.hidden=NO;
        self.mapBtn.hidden=NO;
        self.shareBtn.hidden=NO;
        self.callBtn.hidden=NO;
        self.galleryBtn.hidden=NO;
        self.aboutBtn.hidden=NO;
        self.appointmentBtn.hidden = NO;
        self.blogbtn.hidden = NO;
        
        self.mapBtn.frame = CGRectOffset(self.mapBtn.frame, 0, -60);
        self.galleryBtn.frame = CGRectOffset(self.galleryBtn.frame, 0, 60);
        self.blogbtn.frame = CGRectOffset(self.blogbtn.frame, 0, -60);
        self.offersBtn.frame = CGRectOffset(self.offersBtn.frame, -20, -60);
        self.shareBtn.frame = CGRectOffset(self.shareBtn.frame, 20, -60);
        self.callBtn.frame = CGRectOffset(self.callBtn.frame, -20, 60);
        self.aboutBtn.frame = CGRectOffset(self.aboutBtn.frame, 20, 60);
        self.appointmentBtn.frame = CGRectOffset(self.appointmentBtn.frame, 20, 60);
        
        self.offerLbl.hidden=NO;
        self.mapLbl.hidden=NO;
        self.lblBlog.hidden = NO;
        self.shareLbl.hidden=NO;
        self.callLbl.hidden=NO;
        self.galleryLbl.hidden=NO;
        self.aboutLbl.hidden=NO;
        self.appointmentLbl.hidden=NO;
        
        
        self.offerLbl.alpha     = 0;
        self.mapLbl.alpha       = 0;
        self.lblBlog.alpha      = 0;
        self.shareLbl.alpha     = 0;
        self.callLbl.alpha      = 0;
        self.galleryLbl.alpha   = 0;
        self.aboutLbl.alpha     = 0;
        self.appointmentLbl.alpha   = 0;
        
        
        [UIView animateWithDuration:0.2 delay:0.8 options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.offerLbl.alpha = 1;
                             self.mapLbl.alpha = 1;
                             self.lblBlog.alpha = 1;
                             self.shareLbl.alpha = 1;
                             self.callLbl.alpha = 1;
                             self.galleryLbl.alpha = 1;
                             self.aboutLbl.alpha = 1;
                             self.appointmentLbl.alpha= 1;
                             
                         }
                         completion:nil];
        //flag=0;
        
        //self.animationShowHideImg.image=[UIImage imageNamed:@"home_0009_wrongicon.png"];
        
        [self.animationPlusImg setHidden:YES];
        [self.animationShowHideImg setHidden:NO];
        
        [self.plusBtn setHidden:YES];
        [self.closeBtn setHidden:NO];
    }];
}
#pragma mark - Background Images
- (void)getBGImage {
    nativeclass=[[UrlClass alloc]initWithSelector:@selector(apiResponse1) WithCallBackObject:self];
    
    NSString *baseURL = [NSString stringWithFormat:@"http://glapprapps.com/apps/api/backgroudimagechange?app_id=%@",kMainCompanyID];
    [nativeclass HttpsGetFromServer:baseURL];
}

#pragma mark - UIAlertView
-(void)showAlert:(NSString *)message
{
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"adzmania" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
}

-(void) showAlertWithAction:(NSString *) message {
    msgStr=message;
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:msgStr delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    [alertview show];
}

#pragma mark - UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *str=[alertView buttonTitleAtIndex:buttonIndex];
    
    if ([str isEqualToString:@"Call"]) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",msgStr]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else
        {
            [self showAlert:@"Call facility is not available!!!"];
        }
    }
}

#pragma mark - Api Response
-(void)apiResponse1
{
    NSLog(@" Response%@",nativeclass.Apikeyresponse);
    NSDictionary*jsonData=[nativeclass.Apikeyresponse JSONValue];
    NSLog(@"%@", jsonData);
    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[jsonData valueForKey:kCompnayBG]]];
    NSData *DataImg = [NSData dataWithContentsOfURL:imageUrl];
    [self.appBackgroundView setImage:[UIImage imageWithData:DataImg]];
    //[self.appBackgroundView setValue:[jsonData valueForKey:kCompnayBG]];
   
}

@end

