//
//  OffersViewController.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "OffersViewController.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"
#import "UrlClass.h"
#import "paraClass.h"
#import "JSON.h"

@interface OffersViewController () <UITableViewDataSource,UITableViewDelegate, SDWebImageManagerDelegate>
{
    NSArray *recipePhotosArr;
    UrlClass *nativeclass;
    
    NSUserDefaults *userdefaults;
}
@property (strong, nonatomic) IBOutlet UITableView *offerTable;
@property (weak, nonatomic) IBOutlet UILabel *mOfferText;
@property (weak, nonatomic) IBOutlet UILabel *mOfferLink;

@property (weak, nonatomic) IBOutlet UIImageView *mOfferImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) IBOutlet UIButton *sidebarButton;
//@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@end

@implementation OffersViewController

#pragma mark - viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [self.appDelegate getAdzmaniaData];
    [self setOfferData];
    
    if([self.appDelegate.isGoesFromNotification isEqualToString: @"YES"]) {
       self.appDelegate.animateAgain=@"YES";
    }
    
    else if([self.appDelegate.isGoesFromNotification isEqualToString: @"NO"]) {
        self.appDelegate.animateAgain=@"NO";
    }
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager cancelForDelegate:self];
}

-(void)viewWillAppear:(BOOL)animated {
    [self setOfferData];
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Api Call
-(void) setOfferData {
    
    recipePhotosArr = [[NSMutableArray alloc]init];
    
    userdefaults = [NSUserDefaults standardUserDefaults];
    
    if([[userdefaults valueForKey:@"notified"] isEqualToString:@"yes"]) {
        if([[userdefaults valueForKey:@"contentMotified"] isEqualToString:@"yes"]) {
            if(self.appDelegate.adzManiaData != nil) {
                recipePhotosArr = [NSMutableArray arrayWithObjects:self.appDelegate.userInfoDict,self.appDelegate.adzManiaData, nil];
            
                [userdefaults setObject:recipePhotosArr forKey:@"offerArray"];
                [userdefaults setObject:@"yes" forKey:@"notified"];
                [userdefaults setObject:@"no" forKey:@"contentMotified"];
            }
        }
        
        else {
            recipePhotosArr = [NSMutableArray arrayWithArray:[userdefaults objectForKey:@"offerArray"]];
        }
    }
    
    else if([self.appDelegate.isGoesFromNotification isEqualToString: @"NO"]) {
        [self.indicator setHidden:YES];
        [self.mOfferText setText:[self.appDelegate.adzManiaData valueForKey:kSpecialOfferText]];
        [self.mOfferLink setText:[self.appDelegate.adzManiaData valueForKey:kSpecialOfferUrl]];
        NSURL *url = [NSURL URLWithString:[self.appDelegate.adzManiaData valueForKey:kSpecialOfferImage]];
        [self.mOfferImage setImageWithURL:url];
    }
}

#pragma mark - UIButton Touch
- (IBAction)action_back_home:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView DataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int rounded = roundf((self.view.frame.size.width *240)/320);
    return rounded;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    userdefaults = [NSUserDefaults standardUserDefaults];
    
    if([[userdefaults valueForKey:@"notified"] isEqualToString:@"yes"]) {
        count = 2;
    }
    
    else if([self.appDelegate.isGoesFromNotification isEqualToString: @"NO"]) {
        count = 1;
    }
    
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"offerCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *offerImageView = (UIImageView *)[cell viewWithTag:200];
    UIImageView *shopImageView = (UIImageView *)[cell viewWithTag:201];
    UILabel *itemLbl = (UILabel *)[cell viewWithTag:202];
    UILabel *priceLbl = (UILabel *)[cell viewWithTag:203];
    //UILabel *discountLbl = (UILabel *)[cell viewWithTag:204];
    UIActivityIndicatorView *loadingView = (UIActivityIndicatorView *)[cell viewWithTag:204];
    
    [loadingView startAnimating];
    
    priceLbl.layer.cornerRadius = 10;
    priceLbl.layer.borderWidth = 1;
    priceLbl.layer.borderColor = [[UIColor colorWithRed:136.0f/256.0f green:0.0f/256.0f blue:0.0f/256.0f alpha:1.0] CGColor];
    
    shopImageView.layer.cornerRadius = shopImageView.bounds.size.width/2;
    shopImageView.layer.masksToBounds = YES;
    
    //    UIImage *img= [UIImage imageNamed:[recipePhotosArr objectAtIndex:indexPath.row]];
    //    shopImageView.image = img;
    //    offerImageView.image = img;
    
    NSString *iStr, *itemStr;
    userdefaults = [NSUserDefaults standardUserDefaults];
    
    if([[userdefaults valueForKey:@"notified"] isEqualToString:@"yes"]) {
        if(indexPath.row == 0) {
            iStr = [[recipePhotosArr objectAtIndex:indexPath.row] objectForKey:@"offer_image"];
            itemStr = [[recipePhotosArr objectAtIndex:indexPath.row] objectForKey:@"offer_text"];
        }
        
        else if (indexPath.row == 1) {
            iStr = [[recipePhotosArr objectAtIndex:indexPath.row] valueForKey:kSpecialOfferImage];
            itemStr = [[recipePhotosArr objectAtIndex:indexPath.row] valueForKey:kSpecialOfferText];
        }
    }
    
    else if([self.appDelegate.isGoesFromNotification isEqualToString: @"NO"]) {
        iStr = [self.appDelegate.adzManiaData valueForKey:kSpecialOfferImage];
        itemStr = [self.appDelegate.adzManiaData valueForKey:kSpecialOfferText];
    }

    NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        NSLog(@"Found whitespace");
        iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
    NSURL *url = [NSURL URLWithString:iStr];

    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    if (url)
    {
        [manager downloadWithURL:url delegate:self];
    }
    UIImage *cachedImage = [manager imageWithURL:url];
   

    if (cachedImage)
    {
        // the image is cached -> remove activityIndicator from view
//        for (UIView *v in [self subviews])
//        {
//            if ([v isKindOfClass:[UIActivityIndicatorView class]])
//            {
                [loadingView stopAnimating];
//            }
//        }
    }
    
    
    if(![itemStr isEqual: [NSNull null]]) {
        itemLbl.text = itemStr;
    }
    
   [offerImageView setImageWithURL:url];
   [loadingView stopAnimating];
  
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *urlStr, *mainUrl;

    mainUrl = @"";
    urlStr = [self.appDelegate.adzManiaData valueForKey:kSpecialOfferUrl];

    if([urlStr containsString: @"http"])
    {
        mainUrl = urlStr;
    }
    else
    {
        mainUrl = [NSString stringWithFormat:@"https://%@",urlStr];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mainUrl]];
}

- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image loader:(UIActivityIndicatorView *)loadingView
{
//    self.image = image;
//    
//    for (UIView *v in [self subviews])
//    {
//        if ([v isKindOfClass:[UIActivityIndicatorView class]])
//        {
//            // //NSLog(@"-didFinishWithImage-");
//            [((UIActivityIndicatorView*)v) stopAnimating];
//            [v removeFromSuperview];
//        }
//    }
    
    [loadingView stopAnimating];
    
    [UIView beginAnimations:@"fadeAnimation" context:NULL];
    [UIView setAnimationDuration:0.9];
//    self.alpha = 0;
//    self.image = image;
//    self.alpha=1;
    //[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:YES];
    [UIView commitAnimations];
}


@end

