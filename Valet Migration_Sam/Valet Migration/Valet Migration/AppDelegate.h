//
//  AppDelegate.h
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//
//com.valet.migration
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Constant.h"
#import "IQKeyboardManager.h"
#import "paraClass.h"
#import "JSON.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableDictionary *adzManiaData, *userInfoDict;
@property (strong, nonatomic) NSString *deviceToken;

@property (strong, nonatomic) NSString *animateAgain, *isGoesFromNotification;

+(void) showAlert:(id) delegate withMessage:(NSString *) message;
-(void) getAdzmaniaData;

- (void)saveContext;



@end

