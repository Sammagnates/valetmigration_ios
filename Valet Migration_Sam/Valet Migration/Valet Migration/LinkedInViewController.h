//
//  LinkedInViewController.h
//  Valet Migration
//
//  Created by admin on 26/04/18.
//  Copyright © 2018 Centreonyx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface LinkedInViewController : UIViewController<UIWebViewDelegate,NSURLSessionDelegate>

@end
