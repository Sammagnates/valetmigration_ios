//
//  MapAnnotation.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "MapAnnotation.h"

@implementation MapAnnotation

-(id)initWithTitle:(NSString *)title andCoordinate:

(CLLocationCoordinate2D)coordinate2d
{
    self.title = title;
    
    
    self.coordinate =coordinate2d;
    return self;
}

@end
