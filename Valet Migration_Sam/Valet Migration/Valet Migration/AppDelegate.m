//
//  AppDelegate.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "AppDelegate.h"
#import "DashboardViewController.h"

#import "UrlClass.h"
#import "paraClass.h"
#import "JSON.h"

@import FirebaseInstanceID;
@import FirebaseMessaging;

#import "Firebase.h"
#import <FirebaseCore/FirebaseCore.h>
//#import <FirebaseMessaging/FirebaseMessaging.h>

#import <UserNotifications/UserNotifications.h>   // notifcation for iOS 10
@interface AppDelegate () <FIRMessagingDelegate, UNUserNotificationCenterDelegate>
{
    UrlClass *nativeclass;
}
@end

@implementation AppDelegate
NSString *const kGCMMessageIDKey = @"gcm.message_id";
@synthesize animateAgain, isGoesFromNotification, userInfoDict;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
     [[IQKeyboardManager sharedManager] setEnable:true];
    self.adzManiaData = [[NSMutableDictionary alloc] init];
    animateAgain=@"YES";
    //[self adzManiaData];
    isGoesFromNotification = @"NO";
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];

            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            // For iOS 10 data message (sent via FCM)
            [FIRMessaging messaging].remoteMessageDelegate = self;
#endif
        }

        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];

    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"Token: %@",refreshedToken);
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
    [self connectToFcm];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //[[FIRMessaging messaging] disconnect];
    //NSLog(@"Disconnected from FCM");
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;
- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Valet_Migration"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - found notification
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    if (userInfo != nil) {
        [self parseRemoteNotification:userInfo];
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
}

-(void) parseRemoteNotification:(NSDictionary *) userInfo {
//    NSDictionary *aps = [userInfo valueForKey:@"aps"];
//    if (aps != nil) {
//        NSString *screen = [aps valueForKey:@"screen"];
//        if (screen != nil) {
//            NSString *screenCode = [screen lowercaseString];
//            if ([screenCode isEqualToString:@"as"]) {
//                [self loadViewControllerFromStoryboard:@"aboutusviewcontroller"];
//            } else if ([screenCode isEqualToString:@"ss"]) {
//                [self loadViewControllerFromStoryboard:@"shareviewcontroller"];
//            } else if ([screenCode isEqualToString:@"gs"]) {
//                [self loadViewControllerFromStoryboard:@"galleryviewcontroller"];  //cameraviewcontroller
//            } else if ([screenCode isEqualToString:@"os"]) {
//                [self loadViewControllerFromStoryboard:@"specialoffersviewcontroller"];
//            } else if ([screenCode isEqualToString:@"ms"]) {
//                [self loadViewControllerFromStoryboard:@"mapviewcontroller"];
//            }
//        }
//    }
    userInfoDict = [[NSMutableDictionary alloc]init];
    [userInfoDict addEntriesFromDictionary:userInfo];
    
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setObject:@"yes" forKey:@"notified"];
    [userdefaults setObject:@"yes" forKey:@"contentMotified"];
    
    isGoesFromNotification = @"YES";
    
    [self loadViewControllerFromStoryboard:@"offersviewcontroller"];
}

-(void) loadViewControllerFromStoryboard:(NSString *) identifier {
    UINavigationController *navController = (UINavigationController *) self.window.rootViewController;
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:identifier];
    //[navController pushViewController:vc animated:YES];
     [navController presentViewController:vc animated:YES completion:nil];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}

// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    if (userInfo != nil) {
        [self parseRemoteNotification:userInfo];
    }
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    if (userInfo != nil) {
        [self parseRemoteNotification:userInfo];
    }
    
    completionHandler();
    
//    {
//        aps =     {
//            alert =         {
//                body = "Valet Migration Offer";
//                title = "Valet Migration";
//            };
//            category = "com.valet.migration_TARGET_NOTIFICATION";
//            sound = default;
//        };
//        "gcm.message_id" = "0:1485153267014469%7741208377412083";
//        "offer_image" = "http://www.goofyads.com/admin/uploads/offers/1485153266-valet logo1.png";
//        "offer_text" = "Valet Migration Offer";
//        "offer_url" = "www.valetmigration.com.au";
//        "updated_at" = "{\"date\":\"2017-01-23 06:34:26.000000\",\"timezone\":\"UTC\",\"timezone_type\":3}";
//    }
}
#endif
// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", remoteMessage.appData);
}
#endif
// [END ios_10_data_message_handling]

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
            
            NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
            NSString *isTopicCreated = [defauls objectForKey:@"suscribed"];
            
            if(isTopicCreated == nil) {
                
                NSString *token = [[FIRInstanceID instanceID] token];
                NSLog(@"InstanceID token: %@", token);
                [[FIRMessaging messaging] subscribeToTopic:@"/topics/valet"];
                NSLog(@"Subscribed to valet topic");
                
                [defauls setObject:@"YES" forKey:@"suscribed"];
            }
            
            //NSData *tokenData = [[[FIRInstanceID instanceID] token] dataUsingEncoding:NSUTF8StringEncoding];
            //[[FIRInstanceID instanceID] setAPNSToken:tokenData type:FIRInstanceIDAPNSTokenTypeProd];
            
             [self registerDeviceTokenOnServer:[[FIRInstanceID instanceID] token]];
        }
    }];
}
// [END connect_to_fcm]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs token here.
    // sandbox
    //[[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
    // Production
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
    
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken (%@)", token);
    self.deviceToken = token;
   // [self registerDeviceTokenOnServer:token];
}



#pragma mark - main API
- (void) registerDeviceTokenOnServer:(NSString *) deviceToken {
    //    NSString *requestUrl = [NSString stringWithFormat:@"https://indiansite.com.au/adsmania/services/save_device_token.php?app_id=16&device_token=%@&os_type=1", deviceToken];
    
    //NSString *requestUrl = [NSString stringWithFormat:@"http://wesleyslade.com/adsmania/services/save_device_token.php?app_id=16&device_token=%@&os_type=1", deviceToken];
    
    //e-ksuYETG1k:APA91bG7UmVgmIriW8PqVjp-b-Axbvw5m8Bq78_KatsmI0vpG1iB5HfCwNuhmfdaUGkb3cEy4greu2x4HA36-B-6o3yKW83p6zpiYI5scRKuAcKfrqHJzCGjL_NDOT_K2w_wGVNRB0gs
    
    NSString *requestUrl = [NSString stringWithFormat:@"http://goofyads.com/admin/api/save-token?app_id=%@&device_token=%@&os=1", kMainCompanyID,deviceToken];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:requestUrl]];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error: %@", error.localizedDescription);
            return;
        }
        
        if (data != nil) {
            id resData = [NSJSONSerialization JSONObjectWithData:data  options:0 error:nil];
            NSLog(@"Response: %@",resData);
        }
    }];
    [task resume];
}


-(void) getAdzmaniaData {
    
    nativeclass=[[UrlClass alloc]initWithSelector:@selector(apiResponse) WithCallBackObject:self];
    //[nativeclass HttpsGetFromServer:@"http://glapprapps.com/apps/api/app/143"];
    NSString *strUrl = [NSString stringWithFormat:@"http://glapprapps.com/apps/api/app/%@",kMainCompanyID];
    [nativeclass HttpsGetFromServer:strUrl];
}
+(void)showAlert:(id)delegate withMessage:(NSString *)message
{
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Cav Group Aust" message:message delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
}

#pragma mark - Api Response
-(void)apiResponse
{
    NSLog(@" Response%@",nativeclass.Apikeyresponse);
    NSDictionary*jsonData=[nativeclass.Apikeyresponse JSONValue];
    NSLog(@"%@", jsonData);
    
    [self.adzManiaData setValue:[jsonData valueForKey:kId] forKey:kId];
    [self.adzManiaData setValue:[jsonData valueForKey:kAuthId] forKey:kAuthId];
    [self.adzManiaData setValue:[jsonData valueForKey:kAboutUsDesc] forKey:kAboutUsDesc];
    [self.adzManiaData setValue:[jsonData valueForKey:kAddress] forKey:kAddress];
    [self.adzManiaData setValue:[jsonData valueForKey:kPhoneNumber] forKey:kPhoneNumber];
    [self.adzManiaData setValue:[jsonData valueForKey:kLatGPS] forKey:kLatGPS];
    [self.adzManiaData setValue:[jsonData valueForKey:kLongGPS] forKey:kLongGPS];
    [self.adzManiaData setValue:[jsonData valueForKey:kAppDetails] forKey:kAppDetails];
    [self.adzManiaData setValue:[jsonData valueForKey:kAppName] forKey:kAppName];
    [self.adzManiaData setValue:[jsonData valueForKey:kAppPackageId] forKey:kAppPackageId];
    [self.adzManiaData setValue:[jsonData valueForKey:kEmailId] forKey:kEmailId];
    [self.adzManiaData setValue:[jsonData valueForKey:kUserName] forKey:kUserName];
    [self.adzManiaData setValue:[jsonData valueForKey:kPassword] forKey:kPassword];
    [self.adzManiaData setValue:[jsonData valueForKey:kFacebookUrl] forKey:kFacebookUrl];
    [self.adzManiaData setValue:[jsonData valueForKey:kPinterestUrl] forKey:kPinterestUrl];
    [self.adzManiaData setValue:[jsonData valueForKey:kTwitterUrl] forKey:kTwitterUrl];
    [self.adzManiaData setValue:[jsonData valueForKey:kPictureLimit] forKey:kPictureLimit];
    [self.adzManiaData setValue:[jsonData valueForKey:kInstaURL] forKey:kInstaURL];
        NSString *specialOfferImage = [NSString stringWithFormat:@"%@", [jsonData valueForKey:kSpecialOfferImage]];
        if ([specialOfferImage hasSuffix:@"."]) {
                    specialOfferImage = [specialOfferImage substringWithRange:NSMakeRange(0,[specialOfferImage length] - 1)];
                }
    [self.adzManiaData setValue:specialOfferImage forKey:kSpecialOfferImage];
    [self.adzManiaData setValue:[jsonData valueForKey:kSpecialOfferText] forKey:kSpecialOfferText];
    [self.adzManiaData setValue:[jsonData valueForKey:kSpecialOfferUrl] forKey:kSpecialOfferUrl];
    [self.adzManiaData setValue:[jsonData valueForKey:kTimestamp] forKey:kTimestamp];
    [self.adzManiaData setValue:[jsonData valueForKey:kgalleryImages] forKey:kgalleryImages];
    [self.adzManiaData setValue:[jsonData valueForKey:kCreateTimestamp] forKey:kCreateTimestamp];
    [self.adzManiaData setValue:[jsonData valueForKey:kUpdateTimestamp] forKey:kUpdateTimestamp];

//    if ([[dic objectForKey:@"statuscode"] isEqualToString:@"404"])
//    {
//        //[paraClass AlertView:[dic objectForKey:@"message"]];
//        NSString *msgStr=[dic objectForKey:@"message"];
//        if([msgStr isEqualToString:@"No Record Found"])
//        {
//            self.msgLbl.text=@"No Rides To Show";
//        }
//    }
//    else if([[dic objectForKey:@"statuscode"] isEqualToString:@"201 : verify checking"] && [[dic objectForKey:@"response"] isEqualToString:@"200"])
//    {
//        [paraClass AlertView:[dic objectForKey:@"message"]];
//        
//        //[self dismissViewControllerAnimated:YES completion:nil];
//    }
}

@end
