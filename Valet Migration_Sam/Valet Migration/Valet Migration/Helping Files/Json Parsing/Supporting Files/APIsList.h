//
//  APIsList.h
//  ThilandGolfGuide
//
//  Created by Ramprakash Goyal on 29/05/14.
//  Copyright (c) 2014 Ramprakash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SBJson.h"

#define kDominicBaseURL                         @"http://54.86.186.151/dominic/api/"

#define kDominicSignup                          @"signup.php?"
#define kDominicSignIn                          @"signin.php?"

#define kTHAPIOtherPlaces                       @"/thailandgolf/api/otherplace.php"
#define kTHAPIGolfCourses                       @"/thailandgolf/api/golfcourses.php"
#define kTHAPIHotelandresort                    @"/thailandgolf/api/hotel_resort.php"
#define kTHAPIRestaurents                       @"/thailandgolf/api/restaurant.php"
#define kTHAPISpecialEvents                       @"/thailandgolf/api/special_event.php"
#define kTHAPIArticles                          @"/thailandgolf/api/article.php"
@interface APIsList : NSObject

+ (NSMutableDictionary *)SignUp:(NSMutableDictionary *) Witherror:(NSError **)error;
+ (NSMutableArray *)getRecommendedList:(NSError **)error;
+ (NSMutableArray *)getOtherPlacesList:(NSError **)error;
+ (NSMutableArray *)getHotelAndResortList:(NSError **)error;
+ (NSMutableArray *)getGolfCoursesList:(NSError **)error;
+ (NSMutableArray *)getRestaurentsList:(NSError **)error;
+ (NSMutableArray *)getArticlesList:(NSError **)error;
+ (NSMutableArray *)getSpecialEventsList:(NSError **)error;
@end
