//
//  SohanAlert.m
//  NImeya
//
//  Created by SOHAN LAL on 9/30/11.
//  Copyright 2011 Fox Infosoft. All rights reserved.
//

#import "SohanAlert.h"


@implementation SohanAlert
-(void)showAlert
{
	self.title=@"Please wait...";
	av=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	av.center=CGPointMake(10, 20);
	[self show];

	[av startAnimating];
	[self addSubview:av];

        
    
    
}
-(void)hideAlert
{
	[av stopAnimating];
	[av removeFromSuperview];
    [self dismissWithClickedButtonIndex:0 animated:YES];
    
}



@end
