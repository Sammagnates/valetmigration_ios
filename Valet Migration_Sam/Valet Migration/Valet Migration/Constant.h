//
//  Constant.h
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define kId @"id"
#define kAboutUsDesc @"about_us"
#define kAddress @"address"
#define kPhoneNumber @"phone_number"
#define kLatGPS @"lat_gps"
#define kLongGPS @"long_gps"
#define kAppDetails @"app_details"
#define kAppName @"app_name"
#define kAppPackageId @"app_package_id"
#define kEmailId @"app_email"
#define kUserName @"username"   //
#define kPassword @"password"   //
#define kFacebookUrl @"fb_url"
#define kPinterestUrl @"pin_url"
#define kTwitterUrl @"twitter_url"
#define kPictureLimit @"pic_limit"
#define kInstaURL @"insta_url"  //changes by samir
#define kSpecialOfferImage @"offer_image"
#define kSpecialOfferText @"offer_text"
#define kSpecialOfferUrl @"offer_url"
#define kTimestamp @"timestamp"   //
#define kAuthId @"auth_id"
#define kgalleryImages @"gallery_images"
#define kCreateTimestamp @"created_at"
#define kUpdateTimestamp @"updated_at"
#define kCompnayBG @"bgimagelink"
#define kCompnayBlogUrl @"bloglink"
#define kMainCompanyID @"158" //@"158"



#endif /* Constant_h */
