//
//  BlogVC.m
//  Valet Migration
//
//  Created by manish on 27/07/18.
//  Copyright © 2018 Centreonyx. All rights reserved.
//

#import "BlogVC.h"
#import "UrlClass.h"
#import "paraClass.h"
#import "JSON.h"
#import "Constant.h"

@interface BlogVC ()
{
    int flag;
    NSString *msgStr;
    UrlClass *nativeclass;
}
@property (weak, nonatomic) IBOutlet UIWebView *webViewObj;

@end

@implementation BlogVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self getBlogLinkForCompany];
   
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getBlogLinkForCompany {
    nativeclass=[[UrlClass alloc]initWithSelector:@selector(apiResponse1) WithCallBackObject:self];
    
    NSString *baseURL = [NSString stringWithFormat:@"http://glapprapps.com/apps/api/getbloglink?app_id=%@",kMainCompanyID];
    [nativeclass HttpsGetFromServer:baseURL];
}
#pragma mark - Api Response
-(void)apiResponse1
{
    NSLog(@" Response%@",nativeclass.Apikeyresponse);
    NSDictionary*jsonData=[nativeclass.Apikeyresponse JSONValue];
    NSLog(@"%@", jsonData);
    
    if ([[jsonData valueForKey:@"bloglink"] isEqualToString:@"No Link Available"]) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"adzmania" message:@"No Blog Link Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[jsonData valueForKey:kCompnayBlogUrl]]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageUrl];
    [_webViewObj loadRequest:urlRequest];
    
    //[self.appBackgroundView setValue:[jsonData valueForKey:kCompnayBG]];
    
}
- (IBAction)btnBackClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
