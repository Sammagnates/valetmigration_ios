//
//  AppointmentViewController.m
//  Valet Migration
//
//  Created by admin on 23/04/18.
//  Copyright © 2018 Centreonyx. All rights reserved.
//

#import "AppointmentViewController.h"
#import "AppDelegate.h"
#import "UrlClass.h"
#import "ActionSheetDatePicker.h"

@interface AppointmentViewController (){
    UrlClass *nativeclass;
    NSDate *selectedDate;
    NSDate *selectedTime;
}
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (strong, nonatomic) IBOutlet UITextField *fnameTxt;
@property (strong, nonatomic) IBOutlet UITextField *lnameTxt;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UITextField *mobileTxt;
@property (strong, nonatomic) IBOutlet UITextField *dateTxt;
@property (strong, nonatomic) IBOutlet UITextField *timeTxt;

@end


@implementation AppointmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    self.appDelegate.animateAgain=@"NO";
    
    selectedDate = [NSDate date];
    selectedTime = [NSDate date];
    
    self.registerBtn.layer.cornerRadius = 5;
    self.registerBtn.clipsToBounds = YES;

    [[NSNotificationCenter defaultCenter] removeObserver:@"APIResponseMag"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveApiMagNotification:)
                                                 name:@"APIResponseMag"
                                               object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIAlertView
-(void)showAlert:(NSString *)message
{
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Register" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
}
#pragma mark - Custom method
- (BOOL)validation{
    
    if([[self.fnameTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        [self.fnameTxt becomeFirstResponder];
        [self showAlert:@"Please enter first name.!"];
        return FALSE;
    }
    
    if([[self.lnameTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        
        [self.lnameTxt becomeFirstResponder];
        [self showAlert:@"Please enter last name!"];
        return FALSE;
    }
    
    if([[self.emailTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        
        [self.emailTxt becomeFirstResponder];
        [self showAlert:@"Please enter email!"];
        return FALSE;
    }
    
    if(![self isValidEmail:self.emailTxt.text]){
        
        [self.emailTxt becomeFirstResponder];
        [self showAlert:@"Please enter valid email address!"];
    
    }
    
    if([[self.mobileTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        
        [self.mobileTxt becomeFirstResponder];
        [self showAlert:@"Please enter mobile!"];
        return FALSE;
    }
    
    if([self.mobileTxt.text length] < 10){
        
        [self.mobileTxt becomeFirstResponder];
        [self showAlert:@"Please enter valid mobile!"];
        return FALSE;
    }
    
    if([[self.dateTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        
        [self.dateTxt becomeFirstResponder];
        [self showAlert:@"Please select date of appointment!"];
        return FALSE;
    }
    
    if([[self.timeTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        
        [self.timeTxt becomeFirstResponder];
        [self showAlert:@"Please select time of appointment!"];
        return FALSE;
    
    }
    
    [self.fnameTxt resignFirstResponder];
    [self.lnameTxt resignFirstResponder];
    [self.emailTxt resignFirstResponder];
    [self.mobileTxt resignFirstResponder];
    [self.dateTxt resignFirstResponder];
    [self.timeTxt resignFirstResponder];
    
    return true;
}

-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    self.dateTxt.text = [dateFormatter stringFromDate:selectedDate];
    [self.dateTxt resignFirstResponder];
    
    
}
- (void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    selectedTime = selectedTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    self.timeTxt.text = [dateFormatter stringFromDate:selectedTime];
    [self.timeTxt resignFirstResponder];
}
- (NSString *)urlencode:(NSString *)mainURL {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[mainURL UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

#pragma mark - UIButton Action
- (IBAction)action_register:(id)sender {
    if([self validation]){
        nativeclass = [[UrlClass alloc] initWithSelector:@selector(apiResponse) WithCallBackObject:self];
        
        NSString *baseURL = @"http://glapprapps.com/apps/api/bookmobapi?";
        NSString *auth_id = [self.appDelegate.adzManiaData valueForKey:kAuthId];
        NSString *app_id = [self.appDelegate.adzManiaData valueForKey:kId];
        NSString *firstname = self.fnameTxt.text;
        NSString *lastname = self.lnameTxt.text;
        NSString *email = self.emailTxt.text;
        NSString *mobile = self.mobileTxt.text;
        NSString *date = self.dateTxt.text;
        NSString *time = [self.timeTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *app_email = [self.appDelegate.adzManiaData valueForKey:kEmailId];
        NSString *parameters = [NSString stringWithFormat:@"auth_id=%@&app_id=%@&firstname=%@&lastname=%@&email=%@&mobile=%@&date=%@&time=%@&app_email=%@",auth_id,app_id,firstname,lastname,email,mobile,date,time,app_email];
        NSString *url = [NSString stringWithFormat:@"%@%@",baseURL,parameters];
        
        //NSString *encodedURL = [self urlencode:url];
        [nativeclass HttpsGetFromServer:url];
        
        /*
         // Dictionary with several kay/value pairs and the above array of arrays
         NSDictionary *dict = @{@"auth_id" : auth_id,
         @"app_id" : app_id,
         @"firstname" : firstname,
         @"lastname" : lastname,
         @"email" : email,
         @"mobile" : mobile,
         @"date" : date,
         @"time" : time,
         @"app_email" : app_email
         };
         
         
         // Array of strings
         NSArray *array = @[dict];
         
         NSDictionary *dictA = @{@"data" : array};
         
         NSError *error = nil;
         NSData *json;
         NSString *jsonString = @"";
         // Dictionary convertable to JSON ?
         if ([NSJSONSerialization isValidJSONObject:array])
         {
         json = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
         
         
         // If no errors, let's view the JSON
         if (json != nil && error == nil)
         {
         jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
         
         
         jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
         
         NSLog(@"JSON: %@", jsonString);
         
         [nativeclass HttpsPostToServer:baseURL :jsonString];
         }
         }
         */
    }
}

- (IBAction)action_back_home:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Api Response
-(void)apiResponse
{
    NSLog(@" Response%@",nativeclass.Apikeyresponse);
    NSDictionary*jsonData=[nativeclass.Apikeyresponse JSONValue];
    NSLog(@"%@", jsonData);
    if ([[jsonData objectForKey:@"response"] isEqualToString:@"success"]) {
        self.fnameTxt.text = @"";
        self.lnameTxt.text = @"";
        self.emailTxt.text = @"";
        self.mobileTxt.text = @"";
        self.dateTxt.text = @"";
        self.timeTxt.text = @"";

    }
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Natalie Hillar Comms" message:[jsonData objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
//    [self dismissViewControllerAnimated:YES completion:^{
//
//    }];
}


#pragma mark - UITextFieldDelegate
#pragma mark - TextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    if(textField == self.dateTxt){
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *minimumDateComponents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        [minimumDateComponents setYear:minimumDateComponents.year];
        //NSDate *minDate = [calendar dateFromComponents:minimumDateComponents];
        //NSDate *maxDate = [NSDate date];
         NSDate *minDate = [NSDate date];
        
        
        ActionSheetDatePicker *actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedDate
                                                              minimumDate:minDate
                                                              maximumDate:nil
                                                                   target:self action:@selector(dateWasSelected:element:) origin:textField];
        
       actionSheetPicker.hideCancel = YES;
        [actionSheetPicker showActionSheetPicker];
        [self.fnameTxt resignFirstResponder];
        [self.lnameTxt resignFirstResponder];
        [self.emailTxt resignFirstResponder];
        [self.mobileTxt resignFirstResponder];
        
        [self.timeTxt resignFirstResponder];
        
        return NO;
    }
    else if(textField == self.timeTxt){
        NSInteger minuteInterval = 5;
        //clamp date
        NSInteger referenceTimeInterval = (NSInteger) [selectedTime timeIntervalSinceReferenceDate];
        NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval * 60);
        NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
        if (remainingSeconds > ((minuteInterval * 60) / 2)) {/// round up
            timeRoundedTo5Minutes = referenceTimeInterval + ((minuteInterval * 60) - remainingSeconds);
        }
        
        selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval) timeRoundedTo5Minutes];
        
        ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:selectedTime target:self action:@selector(timeWasSelected:element:) origin:textField];
        datePicker.minuteInterval = minuteInterval;
        [datePicker addCustomButtonWithTitle:@"value" value:[NSDate date]];
        [datePicker addCustomButtonWithTitle:@"sel" target:self selector:@selector(dateSelector:)];
        [datePicker addCustomButtonWithTitle:@"Block" actionBlock:^{
            NSLog(@"Block invoked");
        }];
        [datePicker showActionSheetPicker];
        [self.fnameTxt resignFirstResponder];
        [self.lnameTxt resignFirstResponder];
        [self.emailTxt resignFirstResponder];
        [self.mobileTxt resignFirstResponder];
        [self.dateTxt resignFirstResponder];
        
        
        return NO;
    }
    else {
        return YES;
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
        return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason NS_AVAILABLE_IOS(10_0);
{
    
#ifdef __IPHONE_11_0
    if (@available(iOS 10.0, *)) {
#endif
#ifdef __IPHONE_11_0
    }
#endif
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
