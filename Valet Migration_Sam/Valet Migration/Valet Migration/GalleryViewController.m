//
//  GalleryViewController.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "GalleryViewController.h"

#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"

@interface GalleryViewController () <UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate, SDWebImageManagerDelegate>
{
    NSMutableArray *recipePhotosArr;
    
    int currentIndex, mainCount, cellCount,restObjects,selectedCell;
    int a,b,x,y,countFlag,imgIndex,flag;

    NSIndexPath *currentIndexPathForTable, *previousIndexPath;
    
    NSMutableArray *pattern1Arr,*pattern2Arr,*pattern3Arr,*pattern4Arr,*pattern5Arr,*pattern6Arr;
    
    UITableViewCell *cell;
}

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIButton *sidebarButton;

@property (strong, nonatomic) IBOutlet UICollectionView *galleryCollection;
@property (strong, nonatomic) IBOutlet UIImageView *zoomImg;
@property (strong, nonatomic) IBOutlet UIView *wrapView;

@property (strong, nonatomic) IBOutlet UITableView *galleryTable;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loaderView;
@property (strong, nonatomic) IBOutlet UILabel *loadingLbl;

@property (strong, nonatomic) IBOutlet UILabel *msglbl;

@end

@implementation GalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    self.appDelegate.animateAgain=@"NO";
    
    self.wrapView.hidden=YES;
    
    recipePhotosArr= [[NSMutableArray alloc]init];
    currentIndex = -1;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager cancelForDelegate:self];
    
    UISwipeGestureRecognizer *gestureRight;
    UISwipeGestureRecognizer *gestureLeft;
    gestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    gestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    [gestureLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [[self view] addGestureRecognizer:gestureRight];
    [[self view] addGestureRecognizer:gestureLeft];
    [self getAppPictures];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API Call
- (void) getAppPictures {
    
    recipePhotosArr = [self.appDelegate.adzManiaData valueForKey:kgalleryImages];
    
    NSLog(@"recipePhotosArr: %@",recipePhotosArr);
    
    if(recipePhotosArr.count == 0) {
        
        [self.loaderView stopAnimating];
        self.loadingLbl.hidden=YES;
        
        self.msglbl.hidden = NO;
        self.galleryCollection.hidden = YES;
    }
    
    else {
        
        self.msglbl.hidden = YES;
    }
    
    [self.galleryCollection reloadData];
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return recipePhotosArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"CELL";
    UICollectionViewCell *collCell = [self.galleryCollection dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *arrImg = (UIImageView *)[collCell viewWithTag:100];
    UIActivityIndicatorView *arrActView = (UIActivityIndicatorView *)[collCell viewWithTag:101];
    [arrActView startAnimating];
    
    //NSString *iStr=[recipePhotosArr objectAtIndex:indexPath.row];
    
    NSString *iStr=[[recipePhotosArr objectAtIndex:indexPath.row] objectForKey:@"gallery_path"];
    
    NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        NSLog(@"Found whitespace");
        iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
    NSLog(@"iStr: %@",iStr);
    NSURL *url = [NSURL URLWithString: iStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    if (url)
    {
        [manager downloadWithURL:url delegate:self];
    }
    UIImage *cachedImage = [manager imageWithURL:url];
    //arrImg.image = cachedImage;
    
    if (cachedImage)
    {
        // the image is cached -> remove activityIndicator from view
        //        for (UIView *v in [self subviews])
        //        {
        //            if ([v isKindOfClass:[UIActivityIndicatorView class]])
        //            {
        [arrActView stopAnimating];
        //            }
        //        }
    }
    
    [arrImg setImageWithURL:url];
    [arrActView stopAnimating];
    
    return collCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    currentIndex = (int)indexPath.row;
    
    NSString *iStr=[[recipePhotosArr objectAtIndex:currentIndex] objectForKey:@"gallery_path"];
    NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        NSLog(@"Found whitespace");
        iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
    [self.zoomImg setImageWithURL:[NSURL URLWithString:iStr]];
    self.wrapView.hidden=NO;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    int rounded = roundf((self.view.frame.size.width *98)/320);
    return CGSizeMake(rounded, rounded);
}



- (void)swipeRight:(UISwipeGestureRecognizer *)gesture
{
    currentIndex--;
    if ([recipePhotosArr count] > 0) {
        if ((gesture.state == UIGestureRecognizerStateChanged) ||
            (gesture.state == UIGestureRecognizerStateEnded)) {
            
            if ((currentIndex - 1) < -1) {
                currentIndex = (int) (recipePhotosArr.count - 1);
            }
            
            NSLog(@"currentIndex: %d",currentIndex);
            
            NSString *iStr=[[recipePhotosArr objectAtIndex:currentIndex] objectForKey:@"gallery_path"];
            NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
            if (whiteSpaceRange.location != NSNotFound) {
                NSLog(@"Found whitespace");
                iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            }
            
            [self.zoomImg setImageWithURL:[NSURL URLWithString:iStr]];
        }
    }
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)gesture
{
    currentIndex++;
    if ([recipePhotosArr count] > 0) {
        if ((gesture.state == UIGestureRecognizerStateChanged) ||
            (gesture.state == UIGestureRecognizerStateEnded)) {
            
            if ((currentIndex+1) > recipePhotosArr.count ) {
                currentIndex = 0;
            }
    
            NSLog(@"currentIndex: %d",currentIndex);
            
            NSString *iStr=[[recipePhotosArr objectAtIndex:currentIndex] objectForKey:@"gallery_path"];
            NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
            if (whiteSpaceRange.location != NSNotFound) {
                NSLog(@"Found whitespace");
                iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            }
            
            [self.zoomImg setImageWithURL:[NSURL URLWithString:iStr]];
        }
    }
}

- (IBAction)moveLeft:(id)sender
{
    currentIndex--;
    if ([recipePhotosArr count] > 0) {
        if ((currentIndex - 1) < -1) {
            currentIndex = (int) (recipePhotosArr.count - 1);
        }
        NSLog(@"currentIndex: %d",currentIndex);
        
        NSString *iStr=[[recipePhotosArr objectAtIndex:currentIndex] objectForKey:@"gallery_path"];
        NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        if (whiteSpaceRange.location != NSNotFound) {
            NSLog(@"Found whitespace");
            iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        }
        
        [self.zoomImg setImageWithURL:[NSURL URLWithString:iStr]];
    }
}

- (IBAction)moveRight:(id)sender
{
    currentIndex++;
    if ([recipePhotosArr count] > 0) {
        if ((currentIndex+1) > recipePhotosArr.count ) {
            currentIndex = 0;
        }
        NSLog(@"currentIndex: %d",currentIndex);
        
        NSString *iStr=[[recipePhotosArr objectAtIndex:currentIndex] objectForKey:@"gallery_path"];
        NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        if (whiteSpaceRange.location != NSNotFound) {
            NSLog(@"Found whitespace");
            iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        }
        
        [self.zoomImg setImageWithURL:[NSURL URLWithString:iStr]];
    }
}

#pragma mark - Button Touch
- (IBAction)action_back_home:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)closeTouch:(id)sender {
    self.wrapView.hidden=YES;
}

#pragma mark - Cell Button Touch
-(void)ZoomImg{
    //[self imgBorderWidth:self.zoomImg];
    
    NSString *iStr=[[recipePhotosArr objectAtIndex:currentIndex] objectForKey:@"gallery_path"];
    NSRange whiteSpaceRange = [iStr rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        NSLog(@"Found whitespace");
        iStr = [iStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
    [self.zoomImg setImageWithURL:[NSURL URLWithString:iStr]];
    self.wrapView.hidden = NO;
}

#pragma mark - Method
-(void)imgBorderWidth:(UIImageView *)imageView
{
    //imageView.layer.cornerRadius = 10;
    imageView.layer.borderWidth = 2;
    imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
}

- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image loader:(UIActivityIndicatorView *)loadingView
{
    //    self.image = image;
    //
    //    for (UIView *v in [self subviews])
    //    {
    //        if ([v isKindOfClass:[UIActivityIndicatorView class]])
    //        {
    //            // //NSLog(@"-didFinishWithImage-");
    //            [((UIActivityIndicatorView*)v) stopAnimating];
    //            [v removeFromSuperview];
    //        }
    //    }
    
    [loadingView stopAnimating];
    
    [UIView beginAnimations:@"fadeAnimation" context:NULL];
    [UIView setAnimationDuration:0.9];
    //    self.alpha = 0;
    //    self.image = image;
    //    self.alpha=1;
    //[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:YES];
    [UIView commitAnimations];
}


@end
