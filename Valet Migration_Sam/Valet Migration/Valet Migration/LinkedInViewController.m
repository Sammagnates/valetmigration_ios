//
//  LinkedInViewController.m
//  Valet Migration
//
//  Created by admin on 26/04/18.
//  Copyright © 2018 Centreonyx. All rights reserved.
//

#import "LinkedInViewController.h"

@interface LinkedInViewController (){
    
    NSString *linkedInKey;
    
    NSString *linkedInSecret;
    
    NSString *authorizationEndPoint;
    
    NSString *accessTokenEndPoint;
}
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation LinkedInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    linkedInKey = @"CLIENT_ID";
    
    linkedInSecret = @"CLIENT_SECRET";
    
    authorizationEndPoint = @"https://www.linkedin.com/uas/oauth2/authorization";
    
    accessTokenEndPoint = @"https://www.linkedin.com/uas/oauth2/accessToken";
    
    self.webView.delegate = self;
    [self startAuthorization];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton Action
- (IBAction)actionDismiss:(id)sender {
    [self dismissViewControllerAnimated:true completion: nil];
}

#pragma mark - Custom Functions

-(void)startAuthorization{
    // Specify the response type which should always be "code".
    NSString *responseType = @"code";
    
    // Set the redirect URL. Adding the percent escape characthers is necessary.
    NSString *oauthURL = @"https://com.appcoda.linkedin.oauth/oauth";
    NSString *redirectURL = [oauthURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    // Create a random string based on the time intervale (it will be in the form linkedin12345679).
    NSString *state = [NSString stringWithFormat:@"linkedin%f",[[NSDate date] timeIntervalSince1970]];
    
    // Set preferred scope.
    NSString *scope = @"r_basicprofile";
    
    
    // Create the authorization URL string.
    
    NSString *authorizationURL = [NSString stringWithFormat:@"%@?response_type=%@&client_id=%@&redirect_uri=%@&state=%@&scope=%@",authorizationEndPoint,responseType,linkedInKey,redirectURL,state,scope];
    
    // Create a URL request and load it in the web view.
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:authorizationURL] ];
    [self.webView loadRequest:request];
    
}


-(void)requestForAccessToken:(NSString *)authorizationCode {
    NSString *grantType = @"authorization_code";
    
    NSString *oauthURL = @"https://com.appcoda.linkedin.oauth/oauth";
    NSString *redirectURL = [oauthURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSString *postParams = [NSString stringWithFormat:@"grant_type=%@&code=%@&redirect_uri=%@&client_id=%@&client_secret=%@",grantType,authorizationCode,redirectURL,linkedInKey,linkedInSecret];
    
    // Convert the POST parameters into a NSData object.
    NSData *postData = [postParams dataUsingEncoding:NSUTF8StringEncoding];
    
    
    // Initialize a mutable URL request object using the access token endpoint URL string.
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:accessTokenEndPoint]];
    
    
    // Indicate that we're about to make a POST request.
    request.HTTPMethod = @"POST";
    
    // Set the HTTP body using the postData object created above.
    request.HTTPBody = postData;
    
    // Add the required HTTP header field.
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    
    // Initialize a NSURLSession object.
    NSURLSessionConfiguration *defaultConfigObject=[NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession *session =[NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue:nil];
    
    // Make the request.
    NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error: %@", error.localizedDescription);
            return;
        }
        
        if (data != nil) {
            
            NSLog(@"Response: %@",data);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSInteger statusCode = (long)[httpResponse statusCode];
            
            if (statusCode == 200) {
                // Convert the received JSON data into a dictionary.
                @try {
                    
                    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:data  options:0 error:nil];
                    
                    //let dataDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                    
                    NSString *accessToken = dataDictionary[@"access_token"];
                    
                    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
                    [defauls setObject:accessToken forKey:@"LIAccessToken"];
                    [defauls synchronize];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self dismissViewControllerAnimated:YES completion:nil];
                    });
                    
                    
                }
                @catch (NSException *exception){
                    NSLog(@"Could not convert JSON data into a dictionary.");
                }
            }
            
        }
    }];
    [task resume];
    
    
    
    
}

#pragma mark - webView Delegete
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"Loading URL :%@",request.URL.absoluteString);
    
    //return FALSE; //to stop loading
    
    NSURL *url = [request URL];//request.URL!
    
    if ([url.host isEqualToString:@"com.appcoda.linkedin.oauth"] ) {
        
        
        if (!url.absoluteString  && ([url.absoluteString rangeOfString:@"code"].location != NSNotFound)) {
            NSArray *urlParts = [url.absoluteString componentsSeparatedByString:@"?"];
            
            NSString *code = [urlParts[1] componentsSeparatedByString:@"="][1];
            [self requestForAccessToken:code];
        }
        
    }
    
    return true;
    
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Failed to load with error :%@",[error debugDescription]);
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
