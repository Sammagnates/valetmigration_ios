//
//  MapViewController.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "MapViewController.h"
#import "AppDelegate.h"
#import "MapAnnotation.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController () <MKMapViewDelegate, CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    
    AppDelegate *appDelegate;
    
    double latitude;
    double longitude;
}

@property (weak, nonatomic) IBOutlet UIWebView *mMapView;
@property (weak, nonatomic) IBOutlet UILabel *mAddress;
@property (weak, nonatomic) IBOutlet UILabel *mPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *mEmailId;

@property (strong, nonatomic) IBOutlet MKMapView *mainMapView;

@property (weak, nonatomic) IBOutlet UIView *mapDetailView;

@property (strong, nonatomic)NSString *saddr;
@property (strong, nonatomic)NSString *daddr;

@end

@implementation MapViewController

#pragma mark - viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    appDelegate.animateAgain=@"NO";
    
    //self.mapDetailView.hidden=YES;
    
    latitude = [[appDelegate.adzManiaData valueForKey:kLatGPS] doubleValue];
    longitude = [[appDelegate.adzManiaData valueForKey:kLongGPS] doubleValue];
    
    geocoder = [[CLGeocoder alloc] init];
    locationManager = [[CLLocationManager alloc] init];
    [locationManager startUpdatingLocation];
    NSString *str=[self deviceLocation];
    NSLog(@"%@",str);
    
    self.mainMapView.delegate = self;
    self.mainMapView.centerCoordinate = CLLocationCoordinate2DMake(1.355802, 103.941912);
    self.mainMapView.mapType = MKMapTypeStandard;
    CLLocationCoordinate2D location;
    location.latitude = latitude;
    location.longitude = longitude;
    
    MapAnnotation *newAnnotation = [[MapAnnotation alloc]
                                    initWithTitle:@"" andCoordinate:location];
    [self.mainMapView addAnnotation:newAnnotation];
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Touch
- (IBAction)action_back_home:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CLLocationManagerDelegate
- (NSString *)deviceLocation
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    ////////nslog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    ////////nslog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    [locationManager stopUpdatingLocation];
    // Reverse Geocoding
    ////////nslog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        ////////nslog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0)
        {
            
            placemark =[placemarks lastObject];
            NSLog(@"%@",placemark.locality);
            NSLog(@"%@",placemark.thoroughfare);
            NSLog(@"%@",placemark.subThoroughfare);
            NSLog(@"%@",placemark.subAdministrativeArea);
            
            self.saddr  = [NSString stringWithFormat:@"%@",placemark.thoroughfare];
            if (placemark.thoroughfare != nil)
            {
                ////////nslog(@"ok");
                
            }
            else
            {
                self.saddr  = [NSString stringWithFormat:@"%@",placemark.locality];
                
            }
            ////////nslog(@"%@",saddr);
            
        } else
        {
            ////////nslog(@"%@", error.debugDescription);
        }
    } ];
}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    MKAnnotationView *annotationView = [views objectAtIndex:0];
    id <MKAnnotation> mp = [annotationView annotation];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance
    ([mp coordinate], 1500, 1500);
    [mv setRegion:region animated:YES];
    [mv selectAnnotation:mp animated:YES];
    
    annotationView.canShowCallout = NO;
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    if(annotation != self.mainMapView.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[self.mainMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        //pinView.pinColor = MKPinAnnotationColorGreen;
        pinView.canShowCallout = NO;
        //pinView.animatesDrop = YES;
        pinView.image = [UIImage imageNamed:@"locationSmall.png"];    //as suggested by Squatch
    }
    else {
        [self.mainMapView
         .userLocation setTitle:@"I am here"];
    }
    return pinView;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [self.mAddress setText:[appDelegate.adzManiaData valueForKey:kAddress]];
    [self.mPhoneNumber setText:[appDelegate.adzManiaData valueForKey:kPhoneNumber]];
    [self.mEmailId setText:[appDelegate.adzManiaData valueForKey:kEmailId]];
    
    self.mapDetailView.layer.cornerRadius = 10;
    [view addSubview:self.mapDetailView];
    
    //self.mapDetailView.center=view.center;
    [self.mapDetailView setFrame:CGRectMake(self.mapDetailView.frame.origin.x-60, view.frame.size.height, self.mapDetailView.frame.size.width, self.mapDetailView.frame.size.height)];
    
    self.mapDetailView.hidden=NO;
}

@end
