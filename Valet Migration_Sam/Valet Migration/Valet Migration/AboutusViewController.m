//
//  AboutusViewController.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "AboutusViewController.h"
#import "AppDelegate.h"

@interface AboutusViewController ()

@property (weak, nonatomic) IBOutlet UITextView *mTextView;

@end

@implementation AboutusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.mTextView scrollRangeToVisible:NSMakeRange(0, 1)];
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    appDelegate.animateAgain=@"NO";
    
    NSString *txtAbout = [appDelegate.adzManiaData valueForKey:kAboutUsDesc];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[txtAbout dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    self.mTextView.attributedText = attrStr;
    [self.mTextView setFont:[UIFont systemFontOfSize:15.0f]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)action_back_home:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
