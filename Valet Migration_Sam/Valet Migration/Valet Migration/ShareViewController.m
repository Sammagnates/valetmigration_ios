//
//  ShareViewController.m
//  Valet Migration
//
//  Created by Admin on 07/01/17.
//  Copyright © 2017 Centreonyx. All rights reserved.
//

#import "ShareViewController.h"

#import "AppDelegate.h"

#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface ShareViewController () <MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource,UITableViewDelegate>
{
    NSString *msgStr;
    NSArray *shareArr, *shareImgArr;
}

@property (strong, nonatomic) AppDelegate *appDelegate;

@end

@implementation ShareViewController

 #pragma mark - viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    self.appDelegate.animateAgain=@"NO";
    
    msgStr = @"Download our application from iTunes Store. https://itunes.apple.com/us/app/valet-migration/id1196626449?ls=1&mt=8";
    //msgStr = @"Download our application from iTunes Store."; @""
    
    shareImgArr = [NSArray arrayWithObjects:
                   @"share_0001_fb.png",
                   @"share_0003_twtr.png",
                   @"share_0004_prnt.png",
                   @"share_0003_Insta.png",
                   @"share_0005_mail.png",
                   @"share_0006_sms.png", nil];
    
    shareArr = [NSArray arrayWithObjects:
                @"Facebook",
                @"Twitter",
                @"Pinterest",
                @"Instagram",
                @"Email",
                @"SMS", nil];
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Method
- (IBAction)action_back_home:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)action_post_on_Insta {
    NSURL *pinterestUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.appDelegate.adzManiaData valueForKey:kInstaURL]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:pinterestUrl]) {
        [[UIApplication sharedApplication] openURL:pinterestUrl];
    } else {
        NSLog(@"Pinterest isn't installed.");
        [self showAlert:@"Can't open Twitter page"];
    }
    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.stackoverflow.com"]];
    
}


- (void)action_post_on_facebook {
    NSURL *pinterestUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.appDelegate.adzManiaData valueForKey:kFacebookUrl]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:pinterestUrl]) {
        [[UIApplication sharedApplication] openURL:pinterestUrl];
    } else {
        NSLog(@"Pinterest isn't installed.");
        [self showAlert:@"Can't open Twitter page"];
    }
   // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.stackoverflow.com"]];
    
}
- (void)action_post_email{
    if(![MFMailComposeViewController canSendMail]) {
        [self showAlert:@"Your device cannot send Mail"];
        return;
    }
    
    // Email Subject
    NSString *emailTitle = @"A Great app to download";
    // Email Content
    NSString *messageBody = msgStr;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:[NSString stringWithFormat:@"%@",[self.appDelegate.adzManiaData valueForKey:kEmailId]]];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}
- (void)action_send_message {
    if(![MFMessageComposeViewController canSendText]) {
        [self showAlert:@"Your device doesn't support SMS!"];
        return;
    }
    
    NSArray *recipents = @[@"0755262172"];
    //NSString *message = [NSString stringWithFormat:@"Just sent the %@ file to your email. Please check!", file];
    NSString *message = msgStr;
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}
- (void)action_post_on_twitter {
    
    NSURL *pinterestUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.appDelegate.adzManiaData valueForKey:kTwitterUrl]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:pinterestUrl]) {
        [[UIApplication sharedApplication] openURL:pinterestUrl];
    } else {
        NSLog(@"Pinterest isn't installed.");
        [self showAlert:@"Can't open Twitter page"];
    }
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
//        SLComposeViewController *tweetSheet = [SLComposeViewController
//                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
//        [tweetSheet setInitialText:msgStr];
//
//        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
//
//    else
//    {
//        [self showAlert:@"Please setup your twitter account in Settings."];
//    }
}
- (IBAction)action_post_on_pintrest:(id)sender {
   // NSString *strPinterest = @"pinit12://pinterest.com/pin/create/bookmarklet/?";
    NSURL *pinterestUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.appDelegate.adzManiaData valueForKey:kPinterestUrl]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:pinterestUrl]) {
        [[UIApplication sharedApplication] openURL:pinterestUrl];
    } else {
        NSLog(@"Pinterest isn't installed.");
        [self showAlert:@"Pinterest isn't installed"];
    }
}
-(void)showAlert:(NSString *)message {
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"adzmania" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
    
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"shareArr.count: %d",(int)shareArr.count);
    
    return shareArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        NSString *CellIdentifier = @"shareCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        UIImageView *shareImageView = (UIImageView *)[cell viewWithTag:300];
        UILabel *shareLabel = (UILabel *)[cell viewWithTag:301];
    
        shareImageView.image = [UIImage imageNamed:[shareImgArr objectAtIndex:indexPath.row]];
        shareLabel.text = [shareArr objectAtIndex:indexPath.row];
    
        return cell;
}

#pragma mark - UITableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([[shareArr objectAtIndex:indexPath.row] isEqualToString:@"Facebook"]){
        [self action_post_on_facebook];
    }
    
    else if([[shareArr objectAtIndex:indexPath.row] isEqualToString:@"Twitter"]){
        [self action_post_on_twitter];
    }
    
    else if([[shareArr objectAtIndex:indexPath.row] isEqualToString:@"Pinterest"]){
        [self action_post_on_pintrest:@""];
    }
    
    else if([[shareArr objectAtIndex:indexPath.row] isEqualToString:@"Instagram"]){
        [self action_post_on_Insta];
    }
    
    else if([[shareArr objectAtIndex:indexPath.row] isEqualToString:@"Email"]){
        [self action_post_email];
    }
    
    else if([[shareArr objectAtIndex:indexPath.row] isEqualToString:@"SMS"]){
        [self action_send_message];
    }
}

#pragma mark - MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [self showAlert:@"Failed to send SMS!"];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
